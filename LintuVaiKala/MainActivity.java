package com.lintuvaikala.timo;

import android.R.string;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button tarkistaJasenyysKokonimi;
	EditText etunimi;
	EditText sukunimi;
	String etusuku;
	String kokonimi;
	ImageView image;

	TextView display;
	public Boolean onjasen;
	


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Initialize();
     
        tarkistaJasenyysKokonimi.setOnClickListener(new View.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				
				InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE); 
				inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                           InputMethodManager.HIDE_NOT_ALWAYS);
				
				String etustring =  etunimi.getText().toString().toUpperCase();
				String sukustring = sukunimi.getText().toString().toUpperCase();
				int etupituus = etustring.length();
				int sukupituus = sukustring.length();
				if (etupituus < 2 || sukupituus < 2 || etustring.equals("") || sukustring.equals("")){
					display.setText("etunimi tai sukunimi puuttuu tai liian lyhyt, sy�t� uudelleen!");
					image.setImageResource(R.drawable.kysymysmerkki);
					etustring = "";
					sukustring = "";
				}
				else{
						// TODO Auto-generated method stub
												
						//t�ll� etusuku muuttujalla otetaan koko nimi ja tietokannassa on koko nimi
						etusuku = sukustring + " " + etustring;
						onjasen = false;
						for (int r=0; r<members.length; r++) {
						    	 if (members[r].contains(etustring) && members[r].contains(sukustring)) {
						    		 kokonimi = members[r];
						    		 onjasen = true;
						    	 }
						 }
						
				    	 if (onjasen == true) {
				    		 display.setText(kokonimi + " on j�sen!");
				    		 image.setImageResource(R.drawable.isokalataustakuvaton);
				    	 }else{
				    		 display.setText(etustring + " " + sukustring + " EI ole j�sen!");
				    		 image.setImageResource(R.drawable.isolintutaustakuvaton);
				    	 }
				    	 etunimi.setText("");
				    	 sukunimi.setText("");
				    	 etusuku = "";
				}
				}
		});
      
        
        /**
        checkMembership.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				String etustring =  etunimi.getText().toString().toUpperCase();
				String sukustring = sukunimi.getText().toString().toUpperCase();
				int etupituus = etustring.length();
				int sukupituus = sukustring.length();
				if (etupituus < 3  || sukupituus < 3  || etustring.equals("") || sukustring.equals("")){
					display.setText("etunimi tai sukunimi puuttuu tai liian lyhyt (min 3 merkki� pit�� olla), sy�t� uudelleen");
					etustring = "";
					sukustring = "";
				}
				else{
						// TODO Auto-generated method stub
						//display.setText("painoin nappulaaaaaa");
						//display.setText("etusuku= " + etusuku);
						String etu3 = etustring.substring(0,3);
						String suku3 = sukustring.substring(0,3);
						// t�t� k�ytet��n, jos haluaat tietokantaa ja laittaa siihen vain 3 ensimm�ist� kirjainta etu ja sukunimest�: 
						// etusuku = suku3 + " "+ etu3;
						
						//t�ll� etusuku muuttujalla otetaan koko nimi ja tietokannassa on koko nimi
						etusuku = sukustring + " " + etustring;
						onjasen = false;
						for (int r=0; r<members.length; r++) {
						    	 //System.out.println(members[r][c]);
						    	 //display.setText(members[r][c]);
								display.setText("r="+r+ ": "+members[r]);
								//SystemClock.sleep(2000);
						    	 if (members[r].equals(etusuku)) {
						    		 onjasen = true;
						    	 }
						 }
						
				    	 if (onjasen == true) {
				    		 display.setText(etustring + " " + sukustring + " on j�sen!");
				    	 }else{
				    		 display.setText(etustring + " " + sukustring + " EI ole j�sen!");
				    	 }
				    	 etunimi.setText("");
				    	 sukunimi.setText("");
				    	 etusuku = "";
				}
				}
		});
        */
        
    }

	public void Initialize() {
		// TODO Auto-generated method stub
		// checkMembership = (Button) findViewById(R.id.bCheckMembership);
		tarkistaJasenyysKokonimi = (Button) findViewById(R.id.button1);
		etunimi = (EditText) findViewById(R.id.etEtunimi);
		sukunimi = (EditText) findViewById(R.id.etSukunimi);
		display = (TextView) findViewById(R.id.tvDisplay);
		image = (ImageView) findViewById(R.id.imageView1);

	}
	final String[] members = {
			"VATAJA TIMO",
			"ETUNIMI SUKUNIMI",
			"LASTNAME FIRSTNAME",
			"JONES TIM"

	    };
		
	/**
	static final String members[][] = {

		  {"aaa", "aaa"},
		  {"bbb", "bbb"},

		};
	*/
	



}
